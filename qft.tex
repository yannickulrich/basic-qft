%PDFLATEX
\documentclass[11pt,a4paper]{article}
\usepackage[left=2.5cm,top=3cm,bottom=3cm,right=2.5cm]{geometry}
\newcommand{\D}{\mathrm{d}}
\newcommand{\E}{\mathrm{e}}
\newcommand{\I}{\mathrm{i}}
\newcommand{\io}{\mathrm{i0}^+}
\newcommand{\tr}{\mathrm{tr}}
\usepackage{amsmath}
\usepackage{slashed}
\usepackage{cancel}
\usepackage{framed}
\usepackage{amsfonts}
\usepackage{simpler-wick}
\usepackage{xcolor}
\usepackage{xstring}
\newcommand{\gitcommit}{
    \newread\headfile%
    \openin\headfile=.git/refs/heads/root%
    \read\headfile to\headline%
    \closein\headfile%
    \StrGobbleRight{\headline}{34}%
}
\usepackage[colorlinks]{hyperref}

\definecolor{shadecolor}{rgb}{0.95,0.95,0.95}
\newcommand{\interlude}[1]{\begin{shaded}#1\end{shaded}}

\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\ket}[1]{\left| #1 \right\rangle}
\newcommand{\braket}[3]{\left\langle #1 \middle| #2 \middle| #3 \right\rangle}
\newcommand{\branket}[2]{\left\langle #1 \middle| #2 \right\rangle}

\renewcommand{\vec}{\boldsymbol}
\usepackage{authblk}
\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing,positioning,decorations.pathreplacing}
\usetikzlibrary{decorations.markings}
\tikzset{
    photon/.style={decorate, decoration={snake,amplitude=1pt,segment length=6pt}},
    fermion/.style={postaction={decorate},
        decoration={markings,mark=at position .55 with {\arrow{>}}}},
}
\def\centerarc[#1](#2,#3)(#4:#5:#6)% Syntax: [draw options] (center) (initial angle:final angle:radius)
    { \draw[#1] ({#2+#6*cos(#4)},{#3+#6*sin(#4)}) arc (#4:#5:#6); }

\makeatletter
% Footer and header of the body
% the command ``\pagestyle{myplain}'' must be inserted
% just after ``\begin{document}''
\newcommand\ps@myplain{
\pagenumbering{arabic}
\renewcommand\@oddfoot{\hfill-- \thepage\ --\hfill}
}
\let\ps@plain=\ps@myplain
\makeatother
\setlength{\parindent}{10pt}

\title{\vspace{-1cm}Feynman diagrams without QFT}
\author[a,b]{Y. Ulrich}
\affil[a]{\sl\small Paul Scherrer Institut\protect\\
CH-5232 Villigen PSI, Switzerland\vspace{0.5cm}}


\affil[b]{\sl\small
Physik-Institut, Universit\"at Z\"urich, \protect\\
Winterthurerstrasse 190,
CH-8057 Z\"urich, Switzerland
}


\date{version \gitcommit}

\begin{document}
\pagestyle{myplain}
\maketitle

\vspace{0.5cm}
\begin{center}
\begin{minipage}{13cm}
    \noindent
    \it Please send corrections to \href{mailto:yannick.ulrich@psi.ch}{\tt yannick.ulrich@psi.ch}
or report them online at
\url{https://gitlab.com/yannickulrich/basic-qft/-/issues}
\end{minipage}
\end{center}
\vspace{1cm}


\subsection*{Reminder: Interaction picture}
Let us split the Hamiltonian $H=H_0 + H'$ where $H_0$ is solvable
exactly, i.e. we can find $U_0$ s.t.
\begin{align*}
    \I\frac\D{\D t}U_0 = H_0 U_0\,.
\end{align*}
We now define $U=U_0U'$
\begin{align}
\I\frac\D{\D t} U &= 
    \underbrace{\I\left(\frac\D{\D t}U_0\right)}_{H_0U_0} U'
   +\I U_0 \frac\D{\D t} \stackrel!= HU = (H_0+H')U_0U'\,,
\notag\\\Rightarrow\qquad
\I\frac\D{\D t}U' &= U_0^\dag H' U_0 U' = H_I U'\,,
\label{eq:uint}
\end{align}
defining the {\it interaction picture} $H_I$. We can
solve~\eqref{eq:uint} iteratively
\begin{align*}
U(\infty,-\infty) &= 1-\I\int H_I(\tau)\D\tau 
+ (-\I)^2 \int\D\tau_2\int\D\tau_1 H_I(\tau_2) H_I(\tau_1)+...
\\&
= T\exp\left(-\I\int\D\tau H_I(\tau)\right)
\end{align*}
where $T$ ensures the time-ordering, i.e.
\begin{align*}
(-\I)^2 \int\D\tau_2\int\D\tau_1 H_I(\tau_2) H_I(\tau_1) = 
\frac{(-\I)^2}{2!} T\Big\{ H_I(\tau_2)H_I(\tau_1)\Big\}\,.
\end{align*}

\subsection*{The $S$ matrix in quantum field theory}
The object we want to compute in QFT is the $S$ matrix
\begin{align*}
S = U_I(\infty,-\infty) = T\exp\left(-\I\int\D\tau
H_I(\tau)\right)=1+\I \mathcal{T}
\,,
\end{align*}
where $H_I$ is hopefully small so that the series converges fast
enough. Now, computing $S$ in all generality is obviously impossible.
Let us instead look at the transition amplitude between some initial
state $\ket i$ and some final state $\ket f$. This is
\begin{align*}
\braket fSi = \branket fi - (2\pi)^4 \delta(P_f-P_i) \mathcal{A}_{fi}
\,,
\end{align*}
$\mathcal{A}$ is now the invariant transition amplitude. Squaring it
will give us a matrix element\footnote{Different sources are not
consistent with what is called a matrix element and what an amplitude.
Often $\mathcal{A}$ is called $\mathcal{M}$}.

\interlude{
The $S$ matrix is unitary
\begin{align*}
    1 = \sum_f |\braket fSi|^2 = \sum_f \braket i{S^\dag}f\braket fSi
      = \braket i{S^\dag S}i\,.
\end{align*}
If we split $S=1+\I \mathcal{T}$ we can see
\begin{align*}
    1=SS^\dag &= 1+\I \mathcal{T}-\I \mathcal{T}^\dag+\mathcal{T}\mathcal{T}^\dag\\
    \mathcal{T}-\mathcal{T}^\dag=&2\Im \mathcal{T}=\I \mathcal{T}\mathcal{T}^\dag\,.
\end{align*}
If we calculate
\begin{align*}
    2\braket f{\Im \mathcal{T}}i 
    = \sum_n \braket f{\mathcal{T}^\dag} n\braket n{\mathcal{T}}i
\end{align*}
and set $f=i$, we find the total cross section $i\to$ whatever
\begin{align*}
    \sum_n|\braket n{\mathcal{T}}i|^2 = 2\braket i{\Im \mathcal{T}}i\,.
\end{align*}
}

Now we want to turn this into a relativistic quantum field theory. For
this we turn
\begin{align*}
S = T\exp\left(-\I\int\D\tau H_I(\tau)\right) \to
    T\exp\left(+\I\int\D^4x\ \mathcal{L}_\text{int}(x) \right)\,,
\end{align*}
where $\mathcal{L}_\text{int}$ is the interaction part of the
Lagrangian density.

Let us derive Feynman rules for a toy QFT
\begin{align*}
\mathcal{L} = \frac12(\partial_\mu \phi)^2 - \frac{m_\phi^2}2\phi^2
            + \frac12(\partial_\mu \chi)^2 - \frac{m_\chi^2}2\chi^2
            - g \phi \chi^2
            = \mathcal{L}_0 + \mathcal{L}_\text{int}\,.
\end{align*}
This theory has two particles $\phi$ and $\chi$ that can interact
(later $\phi$ could be the photon and $\chi$ a fermion). We assume
that the coupling $g$ is small.

The $S$ matrix is now
\begin{align}
S = T\exp\left( -\I\int\D^4x\,g\phi\chi^2\right)\,.
\end{align}
$S$ is now an operator in the Fock space, just as $\phi$ and $\chi$!
To understand this, let us look at the free theory.

\subsection*{The free theory}
Let us begin with the free theory $\mathcal{L}_0$. The Euler-Lagrange
equation for $\phi$ or $\chi$ is
\begin{align*}
\partial_\mu \frac{\partial\mathcal{L}_0}{\partial(\partial_\mu\phi)}
- \frac{\partial\mathcal{L}_0}{\partial\phi} = 0\,.
\end{align*}
This gives us the Klein-Gordon equation
\begin{align}
(\partial^2+m_\phi^2)\phi = 0\,.
\end{align}
Let us solve this equation
\begin{align}
\phi(x) &= \int [\D k]\Big( 
    a(\vec k)\E^{-\I k x}+a^\dag(\vec k)\E^{\I k x} \Big)\,,\\
\chi(x) &= \int [\D k]\Big( 
    b(\vec k)\E^{-\I k x}+b^\dag(\vec k)\E^{\I k x} \Big)\,,
\end{align}
where $[dk]$ is the Lorentz invariant phase space measure. The
operators $a(\vec k)$ ($a^\dag(\vec k)$), when acting on the vacuum
$\ket0$ destroy (generate) a particle with momentum $\vec k$. This
means that $\phi(x)$ is also an operator.

The operators $a$ and $b$ have commutation relations like the harmonic
oscillator
\begin{align}
[a(k), a^\dag(q)] &= (2\pi)^3 2\omega_q \delta(\vec q-\vec k) \equiv \delta(q-k)
\,,\\
[a(k), a(q)] &= [a(k),b^\dag(q)] = 0\,.
\end{align}
The $\delta$ function is normalised such that $\int[\D
k]\delta(q-k)=1$.

\interlude{
By defining the canonical momenta
\begin{align*}
\pi=\frac{\partial\mathcal{L}}{\partial\dot\phi}=\dot\phi
\end{align*}
We find equal time commutation relations
\begin{align*}
[\phi(t,\vec x), \pi(t,\vec y)] = \I\delta(\vec x-\vec y)\,.
\end{align*}
}

The objects $\ket i$ and $\bra f$ are now states in the Fock space.
The state $a^\dag(\vec k)\ket 0$ is the state with one $\phi$ particle
with momentum $\vec k$ and mass $m_\phi$. Right now, we do not have
internal degrees of freedom like spin.

The easiest thing to calculate is
\begin{align*}
\tilde G(x) = \braket0{T\{\phi(x)\phi(0)\}}0\,.
\end{align*}
This is the probability of a particle, being created at at $x=0$ and
destroyed at $x$. For simplicity we will assume that $x_0>0$
\begin{align*}
\tilde G(x) = \braket0{\phi(x)\phi(0)}0 = \int[\D k][\D q] \bra0
 \Big(a_k \E^{-\I k x}+ \cancel{a_k^\dag} \E^{\I kx} \Big)
 \Big(\cancel{a_q} + a_q^\dag  \Big) \ket0\,.
\end{align*}
Using
\begin{align}
a_ka_q^\dag\ket0 = [a_k,a_q^\dag]\ket0+a_q^\dag a_k\ket0
 = \delta(k-q)\ket0\label{eq:comvac}
\end{align}
we write
\begin{align*}
\tilde G(x) &= \int[\D k][\D q]\E^{-\I k x}  
        \braket0{a_k a_q^\dag}0
     = \int[\D k]\E^{-\I k x}
    \\&
     = \int[\D k]\Big(
        \theta(x_0)\E^{-\I k x}+\theta(-x_0)\E^{\I k x}
    \Big)\,.
\end{align*}
Let us Fourier transform this
\begin{align}
G(p) = \frac{\I}{p^2-m^2+\io}\,.
\end{align}
This is the {\it Feynman propagator} and also the Green's function of
the Klein-Gordon operator
\begin{align}
(\partial_\mu\partial^\mu+m^2)\tilde G(x) = -\I\delta(x)\,.
\end{align}


\subsection*{The interaction}
Let us now use what we have learned to calculate $\phi\to \chi\chi$,
i.e.
\begin{align*}
\ket i=a^\dag(p_1) \ket0 
\qquad\text{and}\qquad
\ket f=b^\dag(p_2)b^\dag(p_3)\ket0\,.
\end{align*}
The $S$ matrix element is now
\begin{align*}
 \braket iSf &= \cancel{\braket0{b_2b_3a_1^\dag}0} + (-\I g)
    \bra0 b_2b_3\int\D^4x[\D k_4][\D k_5][\D k_6] 
        \Big( a_4\E^{-\I k_4 x}+\cancel{a^\dag_4\E^{\I k_4x}} \Big)
 \\&\qquad
        \Big( \cancel{b_5\E^{-\I k_5 x}}+b^\dag_5\E^{\I k_5x} \Big)
        \Big( \cancel{b_6\E^{-\I k_6 x}}+b^\dag_6\E^{\I k_6x} \Big)
        a_1^\dag \ket0\,.
\end{align*}
Using~\eqref{eq:comvac} we see why the $b_5$ term vanishes
\begin{align*}
\int [\D k_5]\ b_2b_3 \E^{..} b_5b^\dag_6\ket0 = 
\int [\D k_5]\ \E^{..} \delta(k_5-k_6) b_2b_3\ket0 = 0\,.
\end{align*}
We now have and use~\eqref{eq:comvac} again and again
\begin{align*}
 \braket iSf 
&= (-\I g)\int\D^4x[\D k_4][\D k_5][\D k_6] \E^{\I(k_5+k_6-k_4)x}
        \bra0 b_2b_3 b^\dag_5 b^\dag_6
        a_4a_1^\dag \ket0
\\
&= (-\I g)\int\D^4x[\D k_4][\D k_5][\D k_6] \E^{\I(k_5+k_6-k_4)x}
        \delta(p_2-k_6)\delta(p_3-k_5)\delta(k_4-p_1)
            \underbrace{\branket00}_1
\\
&= (-\I g)\int\D^4x\,\E^{\I(p_3+p_2-p_1)x}
=(2\pi)^4\delta(p_2+p_3-p_1)\,(-\I g)\,.
\end{align*}
First note that we have momentum conservation, i.e. $p_1=p_2+p_3$. The
interesting bit is, however, the $(-\I g)$-part. This is a Feynman
rule. This means that for all $\phi\chi\chi$ vertices we have to write
$-\I g$. Note that this is also the coefficient of
$\I\mathcal{L}_\text{int}$, which is how Feynman rules are sometimes
derived.

\interlude{
\subsection*{A more involved example: $2\to2$ scattering}
Let us consider the scattering of $\chi(p_1)\chi^\dag(p_2) \to
\chi(p_3)\chi^\dag(p_4)$. We have
\begin{align*}
\ket i = b_1^\dag d_2^\dag\ket0
\qquad\text{and}\qquad
\bra f=\bra0 b_3 d_4\,.
\end{align*}
We want to calculate
\begin{align*}
T &= (-\I g^2)\frac1{2!} \bra0 b_3d_4 T\Bigg\{
    \int\D^4x_1 
        \underbrace{\phi(x_1)}_{k_5}
        \underbrace{\chi(x_1)}_{k_6}
        \underbrace{\chi^\dag(x_1)}_{k_7}
    \int\D^4x_2
        \underbrace{\phi(x_2)}_{k_8}
        \underbrace{\chi(x_2)}_{k_9}
        \underbrace{\chi^\dag(x_2)}_{k_0}
\Bigg\} b_1^\dag d_2^\dag\ket0\,.
\end{align*}
We now could substitute the expressions for $\phi$ and $\chi$ and
commute all the expressions until we arrive at a result. But we could
also not do that and think about what $\delta$ functions we will get
\begin{align*}
T &= 
(-\I g^2)\frac1{2!} \bra0 
\wick{
b_3d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\chi(x_1)\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\c1\chi(x_2)\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag d_2^\dag
}\ket0
\\&
+
(-\I g^2)\frac1{2!} \bra0 
\wick{
b_3d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\c1\chi(x_1)\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\chi(x_2)\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag d_2^\dag
}\ket0
\\&+
(-\I g^2)\frac1{2!} \bra0 
\wick{
\c1b_3d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\chi(x_1)\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\chi(x_2)\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag d_2^\dag
}\ket0
\end{align*}
where for example $\wick{\c\chi(x_2)\cdots\c{b}_2^\dag}\ket0$
indicates that the contraction would give a $\delta(p_1-k_9)$. We now
have two terms corresponding to two Feynman diagrams. Let us build all
possible contraction involving external particles
\begin{align*}
T 
&= 
(-\I g^2)\frac1{2!} \bra0 
\wick{
\c4b_3\c3d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\c3\chi(x_1)\c4\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\c1\chi(x_2)\c2\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag \c2d_2^\dag
}\ket0
\\&
+
(-\I g^2)\frac1{2!} \bra0 
\wick{
\c3b_3\c4d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\c1\chi(x_1)\c3\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\c4\chi(x_2)\c2\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag \c2d_2^\dag
}\ket0
\\&
+
(-\I g^2)\frac1{2!} \bra0 
\wick{
\c1b_3\c3d_4 T\Bigg\{
    \int\D^4x_1 \phi(x_1)\c3\chi(x_1)\c4\chi^\dag(x_1)
    \int\D^4x_2 \phi(x_2)\c4\chi(x_2)\c2\chi^\dag(x_2)
\Bigg\} \c1b_1^\dag \c2d_2^\dag
}\ket0
\\&
=\int\D^4x_1\D^4x_2\left(
\begin{gathered}
\begin{tikzpicture}
    \node at (-0.5,+0.5)[left] {\tiny$p_1$};
    \node at (-0.5,-0.5)[left] {\tiny$p_2$};
    \draw (-0.5,+0.5) --(0,0) -- (-0.5,-0.5);
    \draw [dashed] 
        node[left] {\tiny$x_2$} 
            (0,0) -- (1,0) 
        node[right] {\tiny$x_1$};
    \draw (1.5,+0.5) --(1,0) -- (1.5,-0.5);
    \node at (1.5,+0.5)[right] {\tiny$p_3$};
    \node at (1.5,-0.5)[right] {\tiny$p_4$};
\end{tikzpicture}
\end{gathered}
+
\begin{gathered}
\begin{tikzpicture}
    \node at (-0.5,+0.5)[left] {\tiny$p_1$};
    \node at (-0.5,-0.5)[left] {\tiny$p_2$};
    \draw (-0.5,+0.5) --(0.5,0.4) -- (1.5,0.5);
    \draw [dashed] (.5,.4) -- (.5,-.4) ;
    \draw (-0.5,-0.5) --(0.5,-0.4) -- (1.5,-0.5);
    \node at (1.5,+0.5)[right] {\tiny$p_3$};
    \node at (1.5,-0.5)[right] {\tiny$p_4$};
    \node at (0.5,+0.4)[above] {\tiny$x_1$};
    \node at (0.5,-0.4)[below] {\tiny$x_2$};
\end{tikzpicture}
\end{gathered}
+
\begin{gathered}
\begin{tikzpicture}
    \node at (-0.5,+0.5)[left] {\tiny$p_1$};
    \node at (-0.5,-0.5)[left] {\tiny$p_2$};
    \draw (-0.5,+0.5) -- (1.5,0.5);
    \draw (-0.5,-0.5) -- (1.5,-0.5);
    \node at (1.5,+0.5)[right] {\tiny$p_3$};
    \node at (1.5,-0.5)[right] {\tiny$p_4$};
    \centerarc[dashed](0.5,-0.5)(180:0:0.5);
    \node at (0,-0.5)[below] {\tiny$x_1$};
    \node at (1,-0.5)[below] {\tiny$x_2$};
\end{tikzpicture}
\end{gathered}
+\cdots
\right)
\end{align*}
All diagrams that are obtained from contracting external particles are
{\it disconnected} and do not contribute to $T$.

What about the internal contraction? Because $\phi$ and $\chi$
commute, we can consider
\begin{align*}
\bra0T\Big\{\cdots\wick{\c\phi(x_1)\cdots\c\phi(x_2)}\cdots\Big\}\ket0
= \bra0T\{\wick{\c\phi(x_1)\c\phi(x_2)}\}\ket0 = \tilde G(x_1-x_2)
\end{align*}
which is just the Feynman propagator. Let us combine what we know
\begin{align*}
T 
&= (-\I g^2)\frac1{2!} \int\D^4x_1\D^4x_2 
    \E^{\I (p_4+p_3)x_1}\E^{-\I(p_1+p_2)x_2}
    \bra0T\{\phi(x_1)\phi(x_2)\}\ket0\\
&+ (-\I g^2)\frac1{2!} \int\D^4x_1\D^4x_2 
    \E^{-\I(p_1-p_3)x_1}\E^{-\I(p_2-p_4)}
    \bra0T\{\phi(x_1)\phi(x_2)\}\ket0
\\
&= (-\I g^2)\frac1{2!} 
    (2\pi)^4\delta(p_1+p_2-p_3-p_4) \frac{\I}{(p_1+p_2)^2-m^2+\io}\\
&+ (-\I g^2)\frac1{2!} 
    (2\pi)^4\delta(p_1+p_2-p_3-p_4) \frac{\I}{(p_1-p_3)^2-m^2+\io}\,.
\end{align*}
By swapping $x_1\leftrightarrow x_2$ we get an additional factor $2$.
\begin{align*}
T = (2\pi)^4\delta(p_1+p_2-p_3-p_4)\times\left(
    (-\I g^2) \frac{\I}{(p_1+p_2)^2-m^2}
   +(-\I g^2) \frac{\I}{(p_1-p_3)^2-m^2}
\right)\,.
\end{align*}

}


We can now formuate a general procedure for this theory
\begin{enumerate}
    \item
    Draw all connected diagrams up to a certain power in $g$

    \item
    Attach directed momenta to each line 

    \item
    For each $\phi\chi\chi$ vertex, attach $-\I g$
    
    \item
    Integrate over all unconstrained momenta

\end{enumerate}

\subsection*{QED}
We now have all the tools to see how QED works\footnote{though like
all QFT lecture, we will speed the discussion up a lot and will not
derive the Feynman rules}. The Lagrangian is
\begin{align*}
\mathcal{L} = -\frac14 F^{\mu\nu}F_{\mu\nu} + 
        \bar\psi(\I\slashed{D}-m)\psi\,.
\end{align*}
The objects here are as follows
\begin{itemize}
    \item
    two (complex) spinor fields $\psi$ and
    $\bar\psi=\psi^\dag\gamma^0$. These are four-component vectors
    containing the spin up and down operators for particles and
    anti-particles. 
    
    \item
    We denote $\slashed{x} = x_\mu\gamma^\mu$. These $\gamma$ matrices
    are $4\times 4$ matrices that fulfill a Cliford algebra, i.e. 
    \begin{align}
    \{\gamma^\mu,\gamma^\nu\} &=
    \gamma^\mu\gamma^\nu+\gamma^\nu\gamma^\mu = 2g^{\mu\nu}
    \label{eq:algebra}
    \end{align}
    \item
    $F_{\mu\nu} = \partial_\mu A_\nu-\partial_\nu A_\mu$ the field
    strength tensor

    \item
    the 4-component vector fields $A_\mu$ of Maxwell

    \item
    the covariant derivative $D_\mu=\partial_\mu-\I eA_\mu$
\end{itemize}
Again, we have the free photon and electron fields
\begin{align*}
\mathcal{L}_0 = -\frac14 F^{\mu\nu}F_{\mu\nu} + 
        \bar\psi(\I\slashed{\partial}-m)\psi
\qquad\text{and}\qquad
\mathcal{L}_\text{int} = e \bar\psi\slashed{A}\psi\,.
\end{align*}
The Euler Lagrange equations for $\mathcal{L}_0$ are the (more or
less) classical Maxwell equations for $A_\mu$ (just keep in mind that
$A_\mu$ is an operator) and the Dirac equation for $\psi$.

The Dirac equation $(\I\slashed{\partial}-m)\psi=0$ has solutions
\begin{align*}
\psi_\alpha = \int[\D k] \sum_{s=1}^2\Big(
    b(s,\vec k)u_\alpha \E^{-\I k x} + d^\dag(s,\vec k)
    v_\alpha(s,\vec k) \E^{\I k x}\Big)\,,
\end{align*}
where $u$ ($v$) describe particles (anti-particles). $s$ indicated the
dependence of the spin-state. It is possible to write $u$ and $v$ down
explictly using $s$ as four-vectors. We will not be doing this and
only note that $u$ and $v$ fulfil the following equation of
motion\footnote{We will from now on drop the spin index dependence}
\begin{align*}
(\slashed{k}-m)u(s,\vec k) = (\slashed{k}+m)v(s,\vec k)=0\,.
\end{align*}

The Green's function give us the propagators
\begin{align*}
\begin{gathered}
\begin{tikzpicture}[baseline=5pt]
\draw [photon] node [left,below] {\small$\nu$} (0,0) -- (1,0)
node[right,below] {\small$\mu$};
\draw[->] (0.25,0.25)--(0.75,0.25) node [above,midway] {\small$q$};
\end{tikzpicture}
\end{gathered}
&= \frac{-\I g_{\mu\nu}}{p^2+\io}\,,\\
\begin{gathered}
\begin{tikzpicture}[baseline=5pt]
\draw[fermion] 
    node [left,below] {\small$\beta$} (0,0) -- (1,0) 
        node[right,below] {\small$\alpha$};
\draw[->] (0.25,0.25)--(0.75,0.25) node [above,midway] {\small$p$};
\end{tikzpicture}
\end{gathered}
 &= \Big[ (\slashed{p}-m)^{-1}\Big]_{\alpha\beta} =
\frac{[\slashed{p}+m]_{\alpha\beta}}{p^2-m^2+\io}\,.
\end{align*}
We can kind of just read of the $e\bar e\gamma$ vertex
\begin{align*}
\begin{gathered}
\begin{tikzpicture}[baseline=5pt]
\draw[fermion] 
    node [left,below] {\small$\beta$} (0,0) -- (0.5,0);
\draw[fermion] (0.5,0) -- (1,0)
        node[right,below] {\small$\alpha$};
\draw[photon] (0.5,0) --+ (0,0.75) node [right] {\small$\mu$};
\end{tikzpicture}
\end{gathered}
 = -\I e\gamma_{\beta\alpha}^\mu\,.
\end{align*}
Finally, we need the asymptotic states. For example an incoming
(outgoing) $e^-$ is
\begin{align*}
\psi b^\dag\ket0 \to u
\qquad\text{and}\qquad
\bra0 b\bar\psi^\dag \to \bar u\,.
\end{align*}
Similarly we use $v$-type spinors for positrons.


\subsection*{Fermi's golden rule}
We now can calculate the amplitude $\mathcal{A}$. However, we want
something like a cross-section. For that we note, that the probability
this process is (up to normalisation)
\begin{align*}
\Big|\mathcal{T}^2\Big|^2 = \Big| \I (2\pi)^4\delta(P_f-P_i) \mathcal{A} \Big|^2
\end{align*}
\interlude{
Unfortunately, $|\delta|^2$ is meaningless. This is because our
amplitude was written in term of plain waves. One way to fix this is
to construct wave-packets. We will instead put the system in a box of
size $L\to\infty$ and consider a time interval $-T<t<T$. The $\delta$
function now becomes
\begin{align*}
(2\pi)^4\delta(P_f-P_i) = I(E_f-E_i,T)I^3(\vec P_f-\vec P_i,L)\,.
\end{align*}
The function $I$ should, for $T\to\infty$ approach the $\delta$
function. A sensible choice is
\begin{align*}
I(\Delta E,T)=\frac2{\Delta E}\sin\frac{\Delta E\ T}2\,,
\end{align*}
that features
\begin{align*}
I(\Delta E,T)^2=2\pi T\delta(\Delta E)\,.
\end{align*}
We now have
\begin{align*}
\Big|(2\pi)^4\delta(P_f-P_i)\Big|^2 \approx
L^3T(2\pi)^4\delta(P_f-P_i)
\end{align*}
Because we normalised the fields with $2E$ particles per volume, we
divide by $2EV = \int u^\dag u$ per particle. The transition rate,
i.e. probability per unit time, is thus
\begin{align*}
\frac1T|\mathcal{A}|^2 V T (2\pi)^4\delta(P_f-P_i)
\prod_i\frac1{2E_iV} \prod_f\frac1{2E_fV}
\end{align*}
where the $i$ ($f$) product runs over initial (final) particles.

Because the box is finite in size there are only
\begin{align*}
\D n=\frac{\D^3\vec k}{(2\pi)^3} V \to \prod_f \frac{\D^3\vec
k_f V}{(2\pi)^3}
\end{align*}
states between $\vec k$ and $\vec k+\D\vec k$. This product now only
runs over final states. The transition rate into a particular part of
phase space is now
\begin{align*}
\D W = |\mathcal{A}|^2 V \prod_\text{in}\frac1{2E_iV} \underbrace{
        (2\pi)^4\delta(P_f-P_i) \prod_f\frac{\D^3\vec k_f}{(2\pi)^32E_fV}
    }_{\D\Phi}
\end{align*}
$\D\Phi$ is now called the Lorentz invariant phase space (which it is
despite its looks). 

For decay rates and cross section $V$ will cancel. Consider a beam of
one particle per $V$ with velocity $v$. It has a flux of $N_0=v/V$.
Now let us generalise to two beams with $\vec v_1$ and $\vec v_2$.
}

The cross section is thus
\begin{align*}
\D\sigma = \frac{\D W}{N_0} = \frac{1}{|\vec v_1-\vec v_2|}
\frac1{4E_1E_2} |\mathcal{A}|^2 \D\Phi\,.
\end{align*}
Note the vectors $\vec v_i$ are added like vectors and not like
relativistic velocities, i.e. $|\vec v_1-\vec v_2|=2$ for massless
particles. For $2\to2$ scattering one can easily show that
\begin{align*}
\frac{\D\sigma}{\D t}=\frac1{64\pi s}\frac1{|\vec p_1|^2}
|\mathcal{A}|^2\,,
\end{align*}
with the Mandelstamm variable $t$ and the centre-of-mass energy $s$.
For massless particles, the $t$ integration goes from $t=0$ to $t=s$
with $|\vec p_1|=\sqrt{s}/2$.


\subsection*{Muon pair production $e^+e^-\to\mu^+\mu^-$}
We are now ready to calculate a matrix element. As an example we
choose muon pair production $e^+e^-\to\mu^+\mu^-$. Muon electron
scattering is then left as an exercise. Let us begin by writing down
the matrix element using the Feynman rules
\begin{align*}
\mathcal{A} = 
\begin{gathered}
\begin{tikzpicture}[scale=0.75]
    \node at (-1,+1) [left] {$e^+(p_1)$};
    \node at (-1,-1) [left] {$e^-(p_2)$};
    \draw[fermion] (-1,-1)--(0,0);
    \draw[fermion] (0,0) -- (-1,1);
    \draw[photon] (0,0) -- (1,0);
    \draw[->](0.25,0.25)--(0.75,0.25) node[midway,above]{\small$q$};
    \draw[fermion,thick] (2,-1)--(1,0);
    \draw[fermion,thick] (1,0) -- (2,1);
    \node at (2,+1) [right] {$\mu^-(p_3)$};
    \node at (2,-1) [right] {$\mu^+(p_4)$};
    \node at (0,0)[left] {\small$\mu$};
    \node at (1,0)[right] {\small$\nu$};
\end{tikzpicture}
\end{gathered}
=
  \bar v(p_1)(-\I e\gamma^\mu) u(p_2) 
        \left(\frac{-\I g_{\mu\nu}}{q^2+\io} \right)
        \bar u(p_3)(-\I e\gamma^\nu) v(p_4)\,.
\end{align*}
We now need $|\mathcal{A}|^2 = \mathcal{A}\mathcal{A}^*$. Because
$\mathcal{A}$ is a complex number $\mathcal{A}^*=\mathcal{A}^\dag$ so
let us just calculate that and see where it leads
\begin{align*}
\mathcal{A}^\dag = \Big(\bar v_1(-\I e\gamma^{\mu'}) u_2 \Big)^\dag
        \left(\frac{+\I g_{\mu'\nu'}}{q^2+\io} \right)
        \Big(\bar u_3(-\I e\gamma^{\nu'}) v_4 \Big)^\dag\,.
\end{align*}
For the spinor line we note that $\bar u=u^\dag \gamma^0$ and that,
under adjungation the order of matrices and vectors reverses
\begin{align*}
\Big(\bar v_1\gamma^{\mu'} u_2 \Big)^\dag =
u_2^\dag (\gamma^{\mu'})^\dag (\gamma^0)^\dag v_1\,.
\end{align*}
Using $(\gamma^\mu)^\dag =\gamma^0\gamma^\mu\gamma^0$,
$(\gamma^0)^\dag=\gamma^0$ as well as $\gamma_0^2 = 1$ we can re-write
this as
\begin{align*}
\Big(\bar v_1\gamma^{\mu'} u_2 \Big)^\dag =
u_2^\dag \gamma^0 \gamma^{\mu'}\gamma^0 \gamma^0 v_1
= \bar u_2\gamma^{\mu'} v_1
\end{align*}
and similarly for the other line. We now have
\begin{align*}
|\mathcal{A}|^2 &= (-\I e)^2(-\I)(+\I e)^2(+\I) 
\frac1{q^4}
\Big(\bar v_1\gamma^\mu u_2\ \bar u_2\gamma^{\mu'} v_1 \Big)
\Big(\bar u_3\gamma_\mu v_4\ \bar v_4\gamma_{\mu'} u_3\Big)\,.
\\&
= \frac{e^4}{q^4}
\Big((\bar v_1)_\alpha\gamma_{\alpha\beta}^\mu (u_2)_\beta\ (\bar u_2)_\delta\gamma_{\delta\rho}^{\mu'} (v_1)_\rho\Big)
\Big((\bar u_3)_\alpha\gamma_{\alpha\beta}^\mu (v_4)_\beta\ (\bar v_4)_\delta\gamma_{\delta\rho}^{\mu'} (u_3)_\rho\Big)\,.
\end{align*}
We now note that $v$ and $u$ form a completeness relation
\begin{align}
\sum_s u_\alpha^s(p)\bar u_\beta^s(p) = (\slashed{p}+m)_{\alpha\beta}
\qquad\text{and}\qquad
\sum_s v_\alpha^s(p)\bar v_\beta^s(p) = (\slashed{p}-m)_{\alpha\beta}\,.
\end{align}
This means we could simplify $|\mathcal{A}|^2$ by summing over final
state spins and averaging over initial states (assuming the experiment
does not measure / prepare these quantities)
\begin{align*}
|\mathcal{A}|^2 &
= \frac{e^4}{q^4}
\Big((\bar v_1)_\alpha\gamma_{\alpha\beta}^\mu (\slashed{p}_2+m)_{\beta\delta}\gamma_{\delta\rho}^{\mu'} (v_1)_\rho\Big)
\Big((\bar u_3)_\alpha\gamma_{\alpha\beta}^\mu (\slashed{p}_4-M)_{\beta\delta}\gamma_{\delta\rho}^{\mu'} (u_3)_\rho\Big)\,.
\\&
= \frac{e^4}{q^4}
\Big((\slashed{p}_1-m)_{\rho\alpha}\gamma_{\alpha\beta}^\mu (\slashed{p}_2+m)_{\beta\delta}\gamma_{\delta\rho}^{\mu'} \Big)
\Big((\slashed{p}_3-M)_{\rho\alpha}\gamma_{\alpha\beta}^\mu (\slashed{p}_4-M)_{\beta\delta}\gamma_{\delta\rho}^{\mu'} \Big)\,.
\end{align*}
This is now the definition of a trace in spinor space $A_{\rho\rho}=\tr A$
\begin{align*}
|\mathcal{A}|^2 &= 
\frac{e^4}{4q^4}
\tr\Big((\slashed{p}_1-m)\gamma^\mu (\slashed{p}_2+m) \gamma^{\mu'}\Big)
\tr\Big((\slashed{p}_3-M)\gamma_\mu (\slashed{p}_4+M) \gamma_{\mu'}\Big)
\,.
\end{align*}
These are now objects we can calculate easily. By
using~\eqref{eq:algebra}.

\interlude{
We need identities for traces of $\gamma$ matrices.
\begin{itemize}
    \item
    $\tr\gamma^\nu=0$: Use~\eqref{eq:algebra} with $\mu=\nu$, i.e.
    $\gamma^\mu\gamma_\mu=4$
    \begin{align*}
        \tr(\gamma^\nu) = \frac14\tr(\gamma^\nu\gamma^\mu\gamma_\mu)
        \stackrel*= -\frac1\tr(\gamma^\mu\gamma^\nu\gamma_\mu)
        =-\frac14\tr(\gamma^\nu\gamma^\mu\gamma_\mu)\,,
    \end{align*}
    where we used~\eqref{eq:algebra} again at $*$.

    \item
    traces of odd numbers of $\gamma$ matrices vanish. 

    \item
    $\tr(\gamma^\mu\gamma^\nu)=4g^{\mu\nu}$. Using~\eqref{eq:algebra}
    and cyclicity
    \begin{align*}
        \tr(\gamma^\mu\gamma^\nu) =
        \frac12\Big(\tr(\gamma^\mu\gamma^\nu)+\tr(\gamma^\nu\gamma^\mu)\Big)
        = \frac12\tr\{\gamma^\mu,\gamma^\nu\} = g^{\mu\nu}\tr1=
        4g^{\mu\nu}
    \end{align*}
    
    \item
    $\tr\Big(\gamma^\mu\gamma^\nu\gamma^\rho\gamma^\sigma\Big)
      =4\Big(g^{\mu\nu}g^{\rho\sigma}-g^{\mu\rho}g^{\nu\sigma}+g^{\mu\sigma}g^{\nu\rho}\Big)$
      similarly
\end{itemize}
}

For $0\sim m\ll M$ we write
\begin{align*}
|\mathcal{A}|^2 
&= 
\frac{e^4}{4q^4}
\tr\Big(\slashed{p}_1\gamma^\mu \slashed{p}_2 \gamma^{\mu'}\Big)
\Big[
\tr\Big(\slashed{p}_3\gamma_\mu \slashed{p}_4 \gamma_{\mu'}\Big)
+M\cancel{\tr\Big(\slashed{p}_3\gamma_\mu \gamma_{\mu'}\Big)}
-M\cancel{\tr\Big(\gamma_\mu \slashed{p}_4 \gamma_{\mu'}\Big)}
-M^2\tr\Big(\gamma_\mu\gamma_{\mu'}\Big)
\Big]
\\
&= 
\frac{4e^4}{q^4}
\Big(p_1^\mu p_2^{\mu'}-p_1\cdot p_2g^{\mu\mu'}+p_1^{\mu'}p_2^{\mu}\Big)
\Big[
\Big(p_3^\mu p_4^{\mu'}-p_3\cdot p_4g^{\mu\mu'}+p_3^{\mu'}p_4^{\mu}\Big)
-M^2g^{\mu\mu'}
\Big]
\\&=
\frac{8e^4}{q^4} \Big(
M^2 p_1\cdot p_2 + p_1\cdot p_4 p_2\cdot p_3 + p_1\cdot p_3 p_2\cdot p_4
\Big)\,.
\end{align*}
Using the Mandelstam variables $s$, $t$ and $u$ as well as $q=p_1+p_2$
we can write
\begin{align*}
p_1\cdot p_2 = \tfrac s2
p_1\cdot p_3 = p_2\cdot p_4 = \tfrac12(M^2-t)
p_1\cdot p_4 = p_2\cdot p_3 = \tfrac12(M^2-u)
\end{align*}
we can now write down the matrix element in terms of experimentally
accessible variables
\begin{align*}
|\mathcal{A}|^2 
&= \frac{4e^4}{s^2} \left( \frac12 (t^2-u^2)+M^2(s-t-u)+M^4\right)
\to \frac{2e^4(t^2+u^2)}{s^2}\,.
\end{align*}
The differential cross section in the high-energy limit is now with
$s+t+u=0$
\begin{align*}
\frac{\D\sigma}{\D t} &=\frac1{16\pi s^2}|\mathcal{A}|^2 =
2\pi\alpha^2\frac{s^2 + 2 s t + 2 t^2}{s^4}
\sigma=\int_0^s\D t\,\frac{\D\sigma}{\D t} =
\frac{16\pi\alpha^2}{3s}\,.
\end{align*}



\subsection*{Acknowledgements and References}
These notes are based on courses given by J. Forshaw, N. Evans and
A. Signer at {\it RAL/STFC School for Young High Energy Physicists}.




\end{document}
