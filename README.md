# QFT notes

These are some notes on basic quantum field theory.
They are meant to give the reader the basic idea behind Feynman diagrams without discussing proper quantisation.

These notes are based on the *RAL/STFC School for Young High Energy Physicists* courses.
